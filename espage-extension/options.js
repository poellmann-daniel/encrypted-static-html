function remove(event) {
    console.log("Removing: ");
    console.log(event.target.dataset.id);
    browser.storage.sync.get("hostkeys", function (value) {
       value.hostkeys.splice(parseInt(event.target.dataset.id), 1);
       console.log(value.hostkeys);
       browser.storage.sync.set({hostkeys: value.hostkeys}, function () {
           location.reload();
       });
    });
}

async function add(e) {
    browser.storage.sync.get("hostkeys", function (value) {
        console.log(value.hostkeys);
        value.hostkeys.push({
            host: document.querySelector("#host").value,
            key: document.querySelector("#key").value
        });
        browser.storage.sync.set({hostkeys: value.hostkeys});
    });
}

async function restoreOptions() {
    browser.storage.sync.get("hostkeys", function (value) {
        console.log(value)
        if (value.hostkeys === undefined || value.hostkeys.length === 0) {
            value.hostkeys = [{host: "https://poellmanndaniel.de/projects/espage/encrypted", key: "nfP6LR1jy0FO3RmzDZbUoU0fmIU+qYl55xomFvaRsuw="}];
            browser.storage.sync.set({hostkeys: value.hostkeys});
        }

        html = "<table><thead><tr><td>Host</td><td>Key</td><td>Remove</td></tr></thead>";
        for (let i = 0; i < value.hostkeys.length; i++) {
            html += "<tr><td>" + value.hostkeys[i].host + "</td><td>"+ value.hostkeys[i].key +"</td><td>" +
                ((value.hostkeys[i].host === "https://poellmanndaniel.de/projects/espage/encrypted") ? "" : "<button data-id='" + i + "' class='remove'>Remove</button>")
                + "</td></tr>";
        }
        html += "</table>"

        document.querySelector("#list").innerHTML = html;

        let removeButtons = document.querySelectorAll(".remove");
        for (var i = 0, len = removeButtons.length; i < len; i++) {
            removeButtons[i].addEventListener("click", remove, false);
        }

    });
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("#add").addEventListener("click", add);

