const ESPageVersion = "v1.0.0";
const header  = "!!ESPage!!:" + ESPageVersion + ":";

/**
 *
 * @param base64
 * @returns {ArrayBufferLike}
 */
function base64ToArrayBuffer(base64) {
  let binary_string;
  binary_string = window.atob(base64);

  const len = binary_string.length;
  const bytes = new Uint8Array(len);
  for (let i = 0; i < len; i++) {
    bytes[i] = binary_string.charCodeAt(i);
  }
  return bytes.buffer;
}

/**
 *
 * @param data
 * @returns {Promise<*>}
 */
const arrayBufferToBase64 = async (data) => {
  const base64url = await new Promise((r) => {
    const reader = new FileReader();
    reader.onload = () => r(reader.result);
    reader.readAsDataURL(new Blob([data]));
  });
  return base64url.split(",", 2)[1];
};

/**
 *
 * @param key
 * @param ctxt
 * @returns {Promise<Uint8Array>}
 */
async function decryptMessage(key, ctxt) {
  let iv = ctxt.slice(0, 16);
  ctxt = ctxt.slice(16);

  try {
    let ptxt = await crypto.subtle.decrypt({name: "AES-GCM", iv: iv}, key, ctxt);
    return new Uint8Array(ptxt);
  } catch (e) {
    console.log("Please check that the key is correct.");
    return undefined;
  }
}

/**
 *
 * @param url
 * @returns {Promise<void>}
 */
async function getKeyForUrl(url) {
  return new Promise(function (resolve, reject) {
    browser.storage.sync.get("hostkeys", function (value) {
      for (let i=0; i < value.hostkeys.length; i++) {
        if (url.startsWith(value.hostkeys[i].host)) {
          console.log("using key: " + value.hostkeys[i].key);
          resolve(value.hostkeys[i].key);
        }
      }
    });
  });
}

/**
 * Checks if key is available for url
 * @param url
 * @returns {Promise<unknown>}
 */
async function isESPage(url) {
  return new Promise(function (resolve, reject) {
    browser.storage.sync.get("hostkeys", function (value) {
      for (let i=0; i < value.hostkeys.length; i++) {
        if (url.startsWith(value.hostkeys[i].host)) {
          resolve(true);
        }
      }
      resolve(false);
    });
  });
}

/**
 * Event Listener that decrypts responses before passing them on
 * @param details
 */
async function listener(details) {
  console.log(details.requestId + ": " + details.url);

  if (!(await isESPage(details.url))) {
    return;
  }

  let filter = browser.webRequest.filterResponseData(details.requestId);
  let decoder = new TextDecoder("utf-8");
  let encoder = new TextEncoder();

  let data = [];
  filter.ondata = event => {
    data.push(event.data);
  };

  filter.onstop = async event => {
    let key;
    try {
      key = await crypto.subtle.importKey("raw", base64ToArrayBuffer(await getKeyForUrl(details.url)), "AES-GCM", true, ["encrypt", "decrypt"]);
    } catch (e) {
      console.log("Error while importing key");
    }
    let str = "";
    for (let buffer of data) {
      str += decoder.decode(buffer, {stream: true});
    }
    str += decoder.decode();

    if (str.startsWith(header)) {
      let dataArray = new Uint8Array(base64ToArrayBuffer(str.slice(header.length)));
      let ptxt = await decryptMessage(key, dataArray);
      filter.write(ptxt);
    } else {
      filter.write(encoder.encode(str));
    }
    filter.close();
  };
}

/**
 * Registers listener for requests
 */
browser.webRequest.onBeforeRequest.addListener(
  listener,
  {urls: ["https://*/*"]},
  ["blocking"]
);
