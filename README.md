# Encrypted Static HTML Pages
[Source on Gitlab](https://gitlab.com/poellmann-daniel/encrypted-static-html)

## Comparison to other tools

|                                    | [ESPage](https://poellmanndaniel.de/projects/espage) | [StatiCrypt](https://robinmoisson.github.io/staticrypt/) | [StatiCrypt Web Crypto API](https://github.com/tarpdalton/staticrypt/tree/webcrypto) | [PageCrypt](https://www.maxlaumeister.com/pagecrypt/) |
|------------------------------------|------------------------------------------------------|----------------------------------------------------------|--------------------------------------------------------------------------------------|-------------------------------------------------------|
| Encryption                         | AES-GCM 256 bit                                      | AES (?presumably CBC, 256 bit?)                          | AES-GCM; SHA256 for key derivation (don't do that!)                                  | AES-GCM 256 bit with PBKDF2                           |
| Encryption Library                 | Web Crypto Subtle                                    | CryptoJS                                                 | Web Crypto Subtle                                                                    | Web Crypto Subtle                                     |
| Browser Support                    | Firefox                                              | All browsers                                             | All browsers                                                                         | All browsers                                          |
| User interaction needed            | once to add key                                      | at least once                                            | at least once                                                                        | every reload                                          | 
| External resources (js/css/images) | Yes, can deal with directories and external content  | Not supported                                            | Not supported                                                                        | Not supported                                         |
| Sync keys between Browsers         | Yes                                                  | No                                                       | No                                                                                   | No                                                    |

Data for the other tools was gathered from their webpages/github. Let me know if I got something incorrect or if it changes in the future.

## Installation

    npm install

## Usage: Encrypt whole directory

    node encrypt.js --input test-html-page --output test-html-page-encrypted [--keyfile <path to keyfile>]

- ```ìnput```: input directory that contains the static webpage
- ```output```: encrypted files are written into this directory (will be created if it does not exist)
- ```keyfile```: optional path to keyfile where the randomly generated key is stored. default=keyfile.txt

Now you can upload your output directory to your webserver.

## Usage: Setting up the addon

- Add the [Firefox Addon ESPage](https://addons.mozilla.org/de/firefox/addon/es-pages/)
- Go to [about:addons](about:addons)
- Add the host prefix (e.g. https://yourdomain.tld/dir/) and the associated key that you used for encryption
The key is the content of the ```--keyfile``` file.

<img src="docs/firefox-addon-settings.png" width="600" />

Now you can access your static encrypted page and it will automatically get decrypted.
See the following example for checking if everything works.

## Example
After installing the add-on, visit [https://poellmanndaniel.de/projects/espage/encrypted/es-page-test.html](https://poellmanndaniel.de/projects/espage/encrypted/es-page-test.html) with the Firefox addon installed. 
You don't need to set a key for this page in the addon-settings as it is already present.

You should see a page similar to:

<img src="docs/screenshot-test-page.png" width="600" />

If you open the page in a different browser (without the add-on), you will only see the ciphertext:

<img src="docs/screenshot-test-page-edge.png" width="600" />

## Firefox Addon
[Firefox Addon Link](https://addons.mozilla.org/de/firefox/addon/es-pages/)

The source for the addon is located in ```espage-extension```. 

## Performance
Pageloads become slower as the add-on will wait until it has received all chunks of data before decrypting them and passing them on to the browser.
(This could be improved s.t. data will be decrypted and passed on to the browser in chunks after checking authenticity.)

## Cryptography
Currently, only ```AES-GCM``` with 256-bit keys is supported.
Both the add-on and the encryption script use [Subtle Crypto API](https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto).

## Encrypted Content Structure

    !!ESPage!!:v<VersionNumber>:base64(IV || AES-GCM-256(key, plaintext))

The addon decides if it should decrypt a reponse based on the ```!!ESPage!!``` header. 
If this header is not present, it will just pass through the response.

## License
MIT License

Copyright (c) 2022 Daniel Poellmann

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
