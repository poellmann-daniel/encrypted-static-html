#!/usr/bin/env bash

cd espage-extension || exit
zip -r -FS ../espage-firefox.zip .
cd ..
mv espage-firefox.zip espage-firefox.xpi