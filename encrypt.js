const fs = require('fs');
const path = require("path")
const { ArgumentParser } = require('argparse')
const toUint8Array = require('base64-to-uint8array');
const {encode, decode} = require('uint8-to-base64');

const crypto = require('node:crypto').webcrypto;

const ESPageVersion = "v1.0.0";
const header  = "!!ESPage!!:" + ESPageVersion + ":";

/**
 * Returns list of all files in a directory
 * @param dirPath
 * @param arrayOfFiles
 * @returns {*[]}
 */
const getAllFiles = function(dirPath, arrayOfFiles=undefined) {
    files = fs.readdirSync(dirPath)

    arrayOfFiles = arrayOfFiles || []

    files.forEach(function(file) {
        if (fs.statSync(dirPath + "/" + file).isDirectory()) {
            arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles)
        } else {
            arrayOfFiles.push(path.join(file))
        }
    })

    return arrayOfFiles
}

/**
 * Encrypts a message with the given AES key (AES-GCM) and returns the (IV || CTXT)
 * IV is randomly chosen
 * @param key
 * @param message
 * @returns {Promise<Uint8Array>}
 */
async function encryptMessage(key, message) {
    let iv = crypto.getRandomValues(new Uint8Array(16));
    let ctxt = new Uint8Array(await crypto.subtle.encrypt({name: "AES-GCM", iv: iv}, key, message));

    const mergedArray = new Uint8Array(iv.length + ctxt.length);
    mergedArray.set(iv, 0);
    mergedArray.set(ctxt, iv.length);

    return mergedArray;
}

/**
 * Decrypts a ciphertext comprised of (IV + ctxt)
 * @param key
 * @param ctxt
 * @returns {Promise<Uint8Array>}
 */
async function decryptMessage(key, ctxt) {
    let iv = ctxt.slice(0, 16);
    ctxt = ctxt.slice(16);

    let ptxt = await crypto.subtle.decrypt(
        {
            name: "AES-GCM",
            iv: iv
        },
        key,
        ctxt
    );
    return new Uint8Array(ptxt);
}

/**
 * Loads the secret key if available from disk or generates a new one and writes it to disk
 * @returns {Promise<*>}
 */
async function getEncryptionKey(keyfile="keyfile.txt") {
    let key;
    if (fs.existsSync(keyfile)) {
        console.log("Reading key from: " + keyfile);
        key = await crypto.subtle.importKey("raw", toUint8Array(fs.readFileSync(keyfile)),"AES-GCM",true,["encrypt", "decrypt"]);
    } else {
        console.log("Generating new encryption key: " + keyfile);
        key = await crypto.subtle.generateKey({name: 'AES-GCM', length: 256}, true, ['encrypt', 'decrypt']);
        let buff = new Buffer(await crypto.subtle.exportKey("raw", key));
        fs.writeFileSync(keyfile, buff.toString('base64'));
    }
    return key;
}

/**
 *
 * @returns {Promise<void>}
 */
async function main() {
    const parser = new ArgumentParser({ description: 'Encrypts static html pages and their dependencies.' });
    parser.add_argument('--input', {type: 'str', help: 'input file or directory' });
    parser.add_argument('--output', {type: 'str', help: 'output file or directory' });
    parser.add_argument('--keyfile', {type: 'str', help: 'output file or directory' });
    let args = parser.parse_args();

    if (!fs.existsSync(args.output)) {
        fs.mkdirSync(args.output, {recursive: true});
    }

    let key = await getEncryptionKey(args.keyfile);

    let files = getAllFiles(args.input);

    for (let i = 0; i < files.length; i++) {
        console.log("Encrypting: " + args.input + "/" + files[i] + " --> " + args.output + "/" + files[i]);

        let ctxt_b64 = header + encode(await encryptMessage(key, new Uint8Array(fs.readFileSync(args.input + "/" + files[i]))));
        fs.writeFileSync(args.output + "/" + files[i], ctxt_b64);

        /*
        let ctxt_written_b64 = fs.readFileSync(args.output + "/" + files[i]).toString();
        let ctxt_written = decode(ctxt_written_b64)
        let ptxt = await decryptMessage(key, ctxt_written);

        let file_content = new Uint8Array(fs.readFileSync(args.input + "/" + files[i]));
        if (!(isEqual(ptxt.buffer, file_content.buffer))) {
            console.log("ERROR");
        }

        fs.writeFileSync("test/" + files[i], ptxt);
        */
    }

    console.log("Done!");
}

main()
